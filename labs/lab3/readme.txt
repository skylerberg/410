Thread 3 took less time than thread 2.
This demonstrates the non-determinism of the performance.
Based on the amount of work that is supposed to be done by each thread (that is, work that is proportional to the thread number), it is clear that thread 3 should take more time than thread 2.
However, the system must have run in such a way that this did not happen.
If I wanted to see that each thread takes more time if it is a later thread, then I would need to run the code many times and take averages (or something like that).
